#include <stdio.h>
#include "device.h"
#include "utility.h"
#include "comutil.h"
#include "mode.h"
#include "servo.h"
#include "ik.h"
#include "walk.h"



void (*mode_funcp[8])(void) = {mode1, mode2, mode3, mode4, mode5, mode6, mode7, mode8};



size_t mode_select(char total_mode){

	unsigned char i = 0;

	delay_ms(100);

	pipipi(i + 1);

	while(1){
		if(buttonB == 0){
			pii(((i + 1) - ((i + 1) % 4)) / 4);
			pipipi((i + 1) % 4);
			break;
		}else if(buttonA == 0){
			i = (i + 1) % total_mode;
			pii(((i + 1) - ((i + 1) % 4)) / 4);
			pipipi((i + 1) % 4);
			while(buttonA == 0);
		}
	}

	return i;
}



void mode1(void)
{
	deploy_legs();
	for (unsigned int i = 0; i < 18; i++) {
		servo_onoff(i, ON);
	}
	delay_s(1);
	for (unsigned int i = 0; i < 18; i++) {
		servo_onoff(i, OFF);
	}
}



void mode2(void)
{
	for (unsigned int i = 0; i < 18; i++) {
		servo_onoff(i, ON);
	}
	deploy_legs();
	wait_servo(2);
	start_walking(40, 40, 40, 600000);
	for (unsigned int i = 0; i < 10; i++)
		walk(40, 40, 40, 600000);
	delay_s(2);
	for (unsigned int i = 0; i < 18; i++) {
		servo_onoff(i, OFF);
	}
}



void mode3(void)
{
	set_all_servo(ON);
	put_legs_in_box(20);
	wait_servo(5);
	set_all_servo(OFF);
}



void mode4(void)
{
	set_all_servo(ON);
	grab_ball(20);
	wait_servo(5);
	delay_s(3);
	set_all_servo(OFF);
}



void mode5(void)
{
	TIM3->CR1 |= TIM_CR1_CEN;
	unsigned char deg = 0;
	while (1) {
		if (buttonA == 0) {
			TIM3->CCR1 -= 1;
			if (deg > 0)
				deg--;
		} else if (buttonB == 0) {
			TIM3->CCR1 += 1;
			if (deg < 180)
				deg++;
		}
		action_servo(0, deg);
		char buf[32];
		snprintf(buf, sizeof(buf), "%d\r\n", deg);
		uputs(buf);
		delay_ms(5);

	}
	TIM3->CR1 &= (uint16_t)~TIM_CR1_CEN;
}



void mode6(void)
{
	set_all_servo(ON);
	deploy_legs();
	delay_s(1);
	warming_up();
	delay_s(2);
	set_all_servo(OFF);
}



void mode7(void)
{
	set_all_servo(ON);
	move_leg_slowly(5, 20, 10, 80, -40, 90, 80, -40 , 45);
	set_all_servo(OFF);
}



void mode8(void)
{
	set_all_servo(ON);
	double first_pos[3] = {80, -40, 90}, last_pos[3] = {80, -40, 50};
	add_command(3, first_pos, last_pos, 500000, 10);
//	add_command(4, first_pos, last_pos, 500000, 30);
//	add_command(5, first_pos, last_pos, 500000, 30);
	exec_command();
	wait_command();
	add_command(4, first_pos, last_pos, 500000, 100);
//	add_command(4, first_pos, last_pos, 500000, 30);
//	add_command(5, first_pos, last_pos, 500000, 30);
	exec_command();
	wait_command();
}
