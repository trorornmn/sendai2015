#include <stdio.h>
#include <stdlib.h>
#include "device.h"
#include "utility.h"
#include "ik.h"
#include "servo_dif.h"
#include "servo.h"
#include "comutil.h"

#define SERVO_SPEED (0.16/60)	// 0.16s/60deg

static TIM_TypeDef *timers[5] = {TIM1, TIM2, TIM3, TIM4, TIM5};

static unsigned char servo_tim[18] = {
	2, 2, 2,
	3, 3, 4,
	5, 5, 5,
	2, 3, 3,
	4, 4, 4,
	5, 1, 1
};
static unsigned char servo_ch[18] = {
	1, 2, 3,
	3, 4, 1,
	1, 2, 3,
	4, 1, 2,
	2, 3, 4,
	4, 1, 4
};

struct servo_data {
	unsigned char deg;
	double time_to_move;
};
static struct servo_data servo_data[18] = {{0}};



static void send_pulse(unsigned int servo_i, uint16_t pulse)
{
	switch (servo_ch[servo_i]) {
		case 1:
			timers[servo_tim[servo_i] - 1]->CCR1 = pulse;
			break;
		case 2:
			timers[servo_tim[servo_i] - 1]->CCR2 = pulse;
			break;
		case 3:
			timers[servo_tim[servo_i] - 1]->CCR3 = pulse;
			break;
		case 4:
			timers[servo_tim[servo_i] - 1]->CCR4 = pulse;
			break;
		default:
			break;
	}
}


void servo_onoff(unsigned int servo_i, enum SERVO_STATUS status)
{
	switch (status) {
		case ON:
			timers[servo_tim[servo_i]-1]->CCER |= (uint16_t)(1 << (servo_ch[servo_i]-1) * 4);
			break;
		case OFF:
			timers[servo_tim[servo_i]-1]->CCER &= (uint16_t)~(1 << (servo_ch[servo_i]-1) * 4);
			break;
	}
}

void action_servo(unsigned int servo_i, unsigned char deg)
{
	unsigned char deg_i = deg / 30;

	if (deg_i == 6) {
		send_pulse(servo_i, servo_dif[servo_i][6]);
	} else {
		double pulse = servo_dif[servo_i][deg_i] + (deg - 30 * deg_i)/30.0*(servo_dif[servo_i][deg_i+1]-servo_dif[servo_i][deg_i]);
		send_pulse(servo_i, (uint16_t)pulse);
	}

	servo_data[servo_i].time_to_move = SERVO_SPEED * abs(deg - servo_data[servo_i].deg);
	servo_data[servo_i].deg = deg;
}



static double calc_waittime(unsigned char first_servo, unsigned char last_servo)
{

	double time_to_wait = servo_data[first_servo].time_to_move;
	servo_data[first_servo].time_to_move = 0;
	for (unsigned char i = (unsigned char)(first_servo+1); i <= last_servo; i++) {
		if (servo_data[i].time_to_move > time_to_wait &&
				servo_data[i].time_to_move != 0)
			time_to_wait = servo_data[i].time_to_move;
		servo_data[i].time_to_move = 0;
	}
	return time_to_wait;
}


void wait_servo(double times)
{
	delay_ms((unsigned int)(calc_waittime(0, 17) * times * 1000));
}




void set_all_servo(enum SERVO_STATUS status)
{
	for (unsigned char i = 0; i < 18; i++) {
		servo_onoff(i, status);
	}
}



void test_servo(unsigned char servo_pin, unsigned char step)
{
	for (unsigned char i = 0; i < 120; i+=step)
	{
		action_servo(servo_pin, i);
		delay_ms(500);
	}
}




struct command commands[6] = {
	{0, 0, 1, 0, 10000, 0, {0, 0, 0}, {0, 0, 0}},
	{0, 0, 1, 0, 10000, 0, {0, 0, 0}, {0, 0, 0}},
	{0, 0, 1, 0, 10000, 0, {0, 0, 0}, {0, 0, 0}},
	{0, 0, 1, 0, 10000, 0, {0, 0, 0}, {0, 0, 0}},
	{0, 0, 1, 0, 10000, 0, {0, 0, 0}, {0, 0, 0}},
	{0, 0, 1, 0, 10000, 0, {0, 0, 0}, {0, 0, 0}}
};

void TIM6_IRQHandler(void)
{
	if (TIM6->SR & 1) {
		TIM6->SR &= (uint16_t)~1;


		for (unsigned int i = 0; i < 6; i++) {
			if (commands[i].flag)
				commands[i].rest_time -= TIM6->ARR*10;
		}

		for (unsigned char i = 0; i < 6; i++) {


			struct command *curcmd;
			curcmd = &commands[i];

			if (!commands[i].flag | !commands[i].move_flag)
				continue;
			if (curcmd->current_step == curcmd->target_step) {
				curcmd->flag = 0;
				curcmd->move_flag = 0;
				continue;
			}

			double next_pos[3];
			calc_next_pos(curcmd->current_step, curcmd->target_step, curcmd->first_pos, curcmd->last_pos, next_pos);

			move_leg(i, next_pos[0], next_pos[1], next_pos[2]);

			curcmd->move_flag = 0;
			curcmd->current_step++;
			curcmd->rest_time = curcmd->target_time/curcmd->target_step;

		}

		int min_wait_leg = -1;
		for (unsigned int i = 0; i < 6; i++)
		{
			if (!commands[i].flag)
				continue;

			if (min_wait_leg == -1)
				min_wait_leg = i;
			else if (commands[i].rest_time < commands[min_wait_leg].rest_time)
				min_wait_leg = i;
		}
		if (min_wait_leg == -1) {
			stop_command();
			return;
		}
		for (unsigned int i = 0; i < 6; i++) {
			if (commands[i].flag && (commands[i].rest_time == commands[min_wait_leg].rest_time))
				commands[i].move_flag = 1;
		}
		TIM6->ARR = (commands[min_wait_leg].rest_time)/10;
	}
}



void add_command(unsigned int leg_pin, const double first_pos[3], const double last_pos[3],  double time, unsigned int step)
{
	commands[leg_pin].flag = 1;
	commands[leg_pin].move_flag = 1;
	commands[leg_pin].target_time = time;
	commands[leg_pin].rest_time = time/step;

	commands[leg_pin].target_step = step;
	commands[leg_pin].current_step = 0;

	for (unsigned char i = 0; i < 3; i++) {
		commands[leg_pin].first_pos[i] = first_pos[i];
		commands[leg_pin].last_pos[i] = last_pos[i];
	}

}


void exec_command(void)
{
	TIM6->CR1 |= 1;
	TIM6->EGR = 1;
}



void stop_command(void)
{
	TIM6->CR1 &= ~1;
}



void wait_command(void)
{
	while (TIM6->CR1&1);
}
