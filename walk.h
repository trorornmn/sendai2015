#ifndef WALK_H
#define WALK_H

void deploy_legs(void);
void put_legs_in_box(unsigned int step);
void warming_up(void);
void start_walking(double x_step, double y_step, double height, double times);
void walk(double x_step, double y_step, double height, double times);
void walk2(double x_step, double y_step, double height, double times);
void grab_ball(double step);

#endif /* WALK_H */
