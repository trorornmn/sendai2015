#ifndef DEVICE_H
#define DEVICE_H

#pragma GCC diagnostic ignored "-Wsign-conversion"
#include <misc.h>
#include <stm32f10x.h>
#include <stm32f10x_gpio.h>
#include <stm32f10x_rcc.h>
#include <stm32f10x_usart.h>
#include <stm32f10x_adc.h>
#include <stm32f10x_tim.h>
#pragma GCC diagnostic warning "-Wsign-conversion"

#include "conversion.h"
#include "port.h"

#endif /* DEVICE_H */
