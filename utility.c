#include "device.h"
#include "utility.h"


static unsigned int delay_ms_count = 0;

void SysTick_Handler(void)
{
	if (delay_ms_count != 0)
		delay_ms_count--;
}



void delay_ms(unsigned int msec)
{
	delay_ms_count = msec;
	SysTick_Config(SystemCoreClock / 1000);

	while (delay_ms_count != 0);

	SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;
}



void delay_s(unsigned int sec)
{
	delay_ms(sec * 1000);
}



void pipipi(char count){

	for (unsigned char i = 0; i < count; i++) {
		delay_ms(60);
		buzzer(1);
		delay_ms(60);
		buzzer(0);
	}
}



void pii(char count){

	for (unsigned char i = 0; i < count; i++) {
		delay_ms(60);
		buzzer(1);
		delay_ms(160);
		buzzer(0);
	}
}



uint16_t read_ADC(uint8_t ADC_Channel)
{
	ADC_RegularChannelConfig(ADC1, ADC_Channel, 1, ADC_SampleTime_55Cycles5); 
	// 変換開始
	ADC_SoftwareStartConvCmd(ADC1,ENABLE) ;
	// A-D変換終了確認処理
	while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);
	// A-Dコンバータから取得した値を返答
	return (ADC_GetConversionValue(ADC1));
}

