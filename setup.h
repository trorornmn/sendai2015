#ifndef SETUP_H
#define SETUP_H

void setup(void);

void enable_periph_clock(void);
void GPIO_setting(void);
void ADC_setting(void);
void USART_setting(void);
void SysTick_setting(void);
void Timer1_setting(void);
void Timer8_setting(void);
void Timer2_setting(void);
void Timer3_setting(void);
void Timer4_setting(void);
void Timer5_setting(void);
void Timer6_setting(void);
void Timer7_setting(void);
void battery_check(void);

void TIM1_IRQHandler(void);
void TIM2_IRQHandler(void);
void TIM3_IRQHandler(void);
void TIM4_IRQHandler(void);
void TIM5_IRQHandler(void);
void TIM6_IRQHandler(void);
void TIM7_IRQHandler(void);

#endif /* SETUP_H */
