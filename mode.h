#ifndef MODE_H
#define MODE_H

#include <stdio.h>
extern void (*mode_funcp[8])(void);

size_t mode_select(char total_mode);

void mode1(void);
void mode2(void);
void mode3(void);
void mode4(void);
void mode5(void);
void mode6(void);
void mode7(void);
void mode8(void);

#endif /* MODE_H */
