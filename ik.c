#include <math.h>
#include "utility.h"
#include "servo.h"
#include "ik.h"

void move_leg_xz(unsigned int leg_i, double x, double z)
{
	double c = lawofcosines(z, x, toradians(90));
	double under_alpha = acos(lawofcosines2(x, z, c));
	
	double alpha = acos(lawofcosines2(TARSUS_LEN, c, 70));
	double beta = acos(lawofcosines2(c, 70, TARSUS_LEN));

	if (isnan(alpha) || isnan(beta)) {
		return;
	}

	if (leg_i < 3) {
		action_servo(leg_i*3+1, (unsigned char)(180-(todegrees(alpha+under_alpha))));
		action_servo(leg_i*3+2, (unsigned char)(todegrees(beta)+30-TARSUS_DEG));
	} else {
		action_servo(leg_i*3+1, (unsigned char)(todegrees(alpha+under_alpha)));
		action_servo(leg_i*3+2, (unsigned char)(180-(todegrees(beta)+30-TARSUS_DEG)));
	}
}



void move_leg(unsigned int leg_i, double x, double y, double z)
{
	if (leg_i >= 3) {
		y = -y;
		x = -x;
	}
	double theta = todegrees(atan2(y, x));
	double theta2 = 90 + theta;

	double len = lawofcosines(x, y, toradians(90)) - FEMURA_LEN;
	action_servo(leg_i*3, (unsigned char)theta2);
	move_leg_xz(leg_i, len, z);
}



void move_leg_slowly(unsigned int leg_i, unsigned int step, double times, double old_x, double old_y, double old_z, double x, double y, double z)
{
	double target_pos[3], old_pos[3] = {old_x, old_y, old_z}, new_pos[3] = {x, y, z};
	for (unsigned int i = 0; i < step; i++) {

		calc_next_pos(i, step, old_pos, new_pos, target_pos);
		move_leg(leg_i, target_pos[0], target_pos[1], target_pos[2]);
		wait_servo(times);
	}
}



void calc_next_pos(unsigned int current_step, unsigned int target_step, const double first_pos[3], const double last_pos[3], double *next_pos)
{
	for (unsigned char i = 0; i < 3; i++) {
		next_pos[i] = ((target_step - current_step) * first_pos[i] + current_step * last_pos[i])/target_step;
	}
}

