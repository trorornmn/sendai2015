# Please specify the path to STM32F10x_StdPeriph_Driver/Libraries
LIBRARIES_PATH	:= ..

COMPILER	:= arm-none-eabi-gcc
DEBUGGER	:= arm-none-eabi-gdb

buildtype	:= debug

CFLAGS		:= -Wall -Wextra -Wno-unused-parameter -Wconversion -std=c99
CFLAGS		+= -mthumb -mcpu=cortex-m3 -march=armv7-m -mfix-cortex-m3-ldrd -lc -lm -lg -lnosys

DEBUG_FLAGS	:= -g3 -O0 -DDEBUG
RELEASE_FLAGS	:= -O0 -DNDEBUG

SOURCES_DIR	:= .
SOURCES_C	:= $(wildcard $(SOURCES_DIR)/*.c)
SOURCES_ASM	:= $(wildcard $(SOURCES_DIR)/*.s)

OBJ_DIR		:= ./obj
OBJECTS		:= $(addprefix $(OBJ_DIR)/$(buildtype)/, $(subst ./,,$(patsubst %.c,%.o,$(SOURCES_C))))
OBJECTS		+= $(addprefix $(OBJ_DIR)/$(buildtype)/, $(subst ./,,$(patsubst %.s,%.o,$(SOURCES_ASM))))

DEP_DIR		:= ./obj
DEPENDS		:= $(addprefix $(DEP_DIR)/$(buildtype)/, $(subst ./,,$(patsubst %.c,%.d,$(SOURCES_C))))


STARTUP_DIR	:= $(LIBRARIES_PATH)/Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x/startup/TrueSTUDIO
STARTUP_ASM	:= $(STARTUP_DIR)/startup_stm32f10x_hd.s
STARTUP_OBJ	:= $(OBJ_DIR)/startup_stm32f10x_hd.o

INCLUDE		:= -I./
INCLUDE		+= -I$(LIBRARIES_PATH)/Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x
INCLUDE		+= -I$(LIBRARIES_PATH)/Libraries/STM32F10x_StdPeriph_Driver/inc
INCLUDE		+= -I$(LIBRARIES_PATH)/Libraries/CMSIS/CM3/CoreSupport

LIBS_DIR	:= -L$(LIBRARIES_PATH)/Libraries/STM32F10x_StdPeriph_Driver/lib
LIBS_DIR	+= -L/opt/gnuarm/arm-none-eabi/lib/armv7-m
LIBS		:= -lstmperiph


TARGET_DIR	:= ./bin
TARGET_NAME	:= hoge
TARGET		:= $(TARGET_DIR)/$(buildtype)/$(TARGET_NAME)

GDB_COMMAND_FILE	:= ./autowrite
GDB_DEBUG_FILE		:= ./autodebug


ifeq ($(buildtype), debug)
	CFLAGS += $(DEBUG_FLAGS)
else ifeq ($(buildtype), release)
	CFLAGS += $(RELEASE_FLAGS)
else
	$(echo buildtype must be debug or release)
	$(exit 1)
endif


all : $(TARGET)


run : $(TARGET) $(GDB_COMMAND_FILE)
	$(DEBUGGER) $(TARGET) -batch -nx -x $(GDB_COMMAND_FILE)

write : $(TARGET) $(GDB_COMMAND_FILE)
	$(DEBUGGER) $(TARGET) -batch -nx -x $(GDB_COMMAND_FILE)

debug : $(TARGET) $(GDB_DEBUG_FILE)
	$(DEBUGGER) $(TARGET) -nx -x $(GDB_DEBUG_FILE)

-include $(DEPENDS)


$(TARGET) : $(OBJECTS) $(STARTUP_OBJ)
	@[ -d `dirname $@` ] || mkdir -p `dirname $@`
	$(COMPILER) $(OBJECTS) $(CFLAGS) $(INCLUDE) $(LIBS_DIR) $(LIBS) $(STARTUP_OBJ) -o $(TARGET) -Tlinker-script.ld


$(OBJ_DIR)/$(buildtype)/%.o : $(SOURCES_DIR)/%.c Makefile
	@[ -d `dirname $@` ] || mkdir -p `dirname $@`
	$(COMPILER) -c -MMD -MP $< $(CFLAGS) $(INCLUDE) $(LIBS_DIR) $(LIBS) -o $@ 


$(OBJ_DIR)/$(buildtype)/%.o : $(SOURCES_DIR)/%.s Makefile
	@[ -d `dirname $@` ] || mkdir -p `dirname $@`
	$(COMPILER) -c -MMD -MP $< $(CFLAGS) $(INCLUDE) $(LIBS_DIR) $(LIBS) -o $@ 

$(STARTUP_OBJ) : $(STARTUP_ASM) Makefile
	@[ -d `dirname $@` ] || mkdir -p `dirname $@`
	$(COMPILER) -c $< $(CFLAGS) -o $@ 

clean :		
	rm -f -r $(TARGET) $(OBJECTS) $(DEPENDS) $(TARGET_DIR) $(OBJ_DIR)

