#include <string.h>
#include <stdarg.h>
#include "device.h"
#include "comutil.h"


uint16_t uputchar(uint16_t c)
{
	USART_SendData(USART1, c);
	while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
	return c;
}



int uputs(const char *s)
{
	int i;
	for (i = 0; s[i] != '\0'; i++) {
		uputchar(s[i]);
	}
	return i;
}


int uprintf(const char *format, ...)
{
	char str[256];
	va_list arg_list;
	va_start(arg_list, format);
	vsnprintf(str, sizeof(str), format, arg_list);
	va_end(arg_list);
	uputs(str);

	return (int)strlen(str);
}
