#ifndef CONVERSION_H
#define CONVERSION_H

#define ADC_to_V(adc)	((double)(adc) / 4095 * 3.3)

#endif /* CONVERSION_H */
