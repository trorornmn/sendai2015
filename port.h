#ifndef PORT_H
#define PORT_H

#define buzzer(x) digital_write(GPIOC, 15, x)
#define buttonA digital_read(GPIOC, 14)
#define buttonB digital_read(GPIOC, 13)

#endif /* PORT_H */
