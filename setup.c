#include <stdio.h>
#include "device.h"
#include "utility.h"
#include "comutil.h"
#include "setup.h"

void setup(void)
{
	GPIO_setting();
	ADC_setting();
	USART_setting();
	SysTick_setting();
	Timer1_setting();
	Timer2_setting();
	Timer3_setting();
	Timer4_setting();
	Timer5_setting();
	Timer6_setting();

	delay_ms(200);

	battery_check();
}



void battery_check(void)
{
	int val = read_ADC(10);
	(void)(val);
}



void GPIO_setting(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA |
			RCC_APB2Periph_GPIOB |
			RCC_APB2Periph_GPIOC |
			RCC_APB2Periph_GPIOD, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);

	GPIO_PinRemapConfig(GPIO_FullRemap_TIM2, ENABLE);
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);

	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Pin = 0b1111111111111111;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	GPIO_Init(GPIOD, &GPIO_InitStructure);

	// Button A and B
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14 | GPIO_Pin_13;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	// USART TX
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	// USART RX
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_Init(GPIOA, &GPIO_InitStructure);


	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_11 | GPIO_Pin_15;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_3 | GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
}



void ADC_setting(void)
{
	RCC_ADCCLKConfig(RCC_PCLK2_Div2);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_StructInit (&GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	ADC_InitTypeDef ADC_InitStructure;
	// A-Dコンバータ初期化準備
	ADC_StructInit(&ADC_InitStructure); 

	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfChannel = 1; 
	ADC_Init(ADC1, &ADC_InitStructure);

	//ADC_ChannelConfig(ADC1, ADC_Channel_5, ADC_SampleTime_7_5Cycles);

	ADC_Cmd(ADC1, ENABLE);

	//A-Dコンバータ1のキャリブレーションを実施
	ADC_ResetCalibration(ADC1);
	while(ADC_GetResetCalibrationStatus(ADC1)) ;
	ADC_StartCalibration(ADC1) ;
	while(ADC_GetCalibrationStatus(ADC1));

}



void USART_setting(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

	USART_InitTypeDef USART_InitStructure;

	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	USART_Init(USART1, &USART_InitStructure);
	// RX interrupt
	// USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	USART_Cmd(USART1, ENABLE);
}



void SysTick_setting(void)
{
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);

	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = SysTick_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK);
	//NVIC_SetPriority(SysTick_IRQn, 5);
}


void Timer1_setting(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);

	TIM1->BDTR = 0b1100000000000000;
	TIM1->CR1 = 0b0010000000;
	TIM1->SMCR = 0;
	TIM1->ARR = 20000 - 1;
	TIM1->PSC = 72;

	TIM1->SR = 0;
	TIM1->DIER = 0;

	TIM1->EGR |= 1;

	TIM1->CCMR1 =	0b0110100001101000;
	TIM1->CCMR2 =	0b0110100001101000;
	TIM1->CCER =	0;

	TIM1->CR1 |= 1;
}



void Timer2_setting(void)
{
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

	TIM2->CR1 = 0b0010000000;
	TIM2->ARR = 20000 - 1;
	TIM2->PSC = 72;

	TIM2->SR = (uint16_t)~1;
	TIM2->DIER = 0;

	TIM2->EGR |= 1;

	TIM2->CCMR1 =	0b0110100001101000;
	TIM2->CCMR2 =	0b0110100001101000;
	TIM2->CCER = 0;

	TIM2->CR1 |= 1;
}


void Timer3_setting(void)
{
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

	TIM3->CR1 = 0b0010000000;
	TIM3->ARR = 20000 - 1;
	TIM3->PSC = 72;

	TIM3->SR = (uint16_t)~1;
	TIM3->DIER = 0;

	TIM3->EGR |= 1;

	TIM3->CCMR1 =	0b0110100001101000;
	TIM3->CCMR2 =	0b0110100001101000;
	TIM3->CCER = 0;

	TIM3->CR1 |= 1;
}




void Timer4_setting(void)
{
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);

	TIM4->CR1 = 0b0010000000;
	TIM4->ARR = 20000 - 1;
	TIM4->PSC = 72;

	TIM4->SR = (uint16_t)~1;
	TIM4->DIER = 0;

	TIM4->EGR |= 1;

	TIM4->CCMR1 =	0b0110100001101000;
	TIM4->CCMR2 =	0b0110100001101000;
	TIM4->CCER = 0;

	TIM4->CR1 |= 1;
}



void Timer5_setting(void)
{
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5, ENABLE);

	TIM5->CR1 = 0b0010000000;
	TIM5->ARR = 20000 - 1;
	TIM5->PSC = 72;

	TIM5->SR = (uint16_t)~1;
	TIM5->DIER = 0;

	TIM5->EGR |= 1;

	TIM5->CCMR1 =	0b0110100001101000;
	TIM5->CCMR2 =	0b0110100001101000;
	TIM5->CCER = 0;

	TIM5->CR1 |= 1;
}



void Timer6_setting(void)
{
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6, ENABLE);

	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = TIM6_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	TIM6->CR1 = 0b10000000;
	TIM6->ARR = 200 - 1;
	TIM6->PSC = 720;
	TIM6->DIER = 0x1;
}


void TIM1_IRQHandler(void)
{
	if (TIM1->SR & 1) {
		TIM1->SR &= (uint16_t)~1;
	}
}



void TIM2_IRQHandler(void)
{
	if (TIM2->SR & 1) {
		TIM2->SR &= (uint16_t)~1;
	}
}



void TIM3_IRQHandler(void)
{
	pipipi(2);
	if (TIM3->SR & 1) {
		TIM3->SR &= (uint16_t)~1;
	}
}



void TIM4_IRQHandler(void)
{
	if (TIM4->SR & 1) {
		TIM4->SR &= (uint16_t)~1;
	}
}



void TIM5_IRQHandler(void)
{
	if (TIM5->SR & 1) {
		TIM5->SR &= (uint16_t)~1;
	}
}





void TIM7_IRQHandler(void)
{
	if (TIM7->SR & 1) {
		TIM7->SR &= (uint16_t)~1;
	}
}
