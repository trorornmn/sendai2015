#include "device.h"
#include "utility.h"
#include "servo.h"
#include "ik.h"
#include "walk.h"

static const double leg_base_position[6][3] = {
	{80, -40, 90},
	{80, 0, 90},
	{80, 40, 90},
	{-80, -40, 90},
	{-80, 0, 90},
	{-80, 40, 90},
//	{80, 40, 120},
//	{80, 0, 120},
//	{80, -40, 120},
//	{80, 40, 120},
//	{80, 0, 120},
//	{80, -40, 120},
};



static const double leg_base_position4[6][3] = {
	//{80, 40, 100},
	//{80, -90, 100},
	//{80, -70, 70},
	//{80, 40, 100},
	//{80, -90, 100},
	//{80, -70, 70},
	{60, 60, 90},
	{60, -60, 90},
	{60, -70, 70},
	{60, 60, 90},
	{60, -60, 90},
	{60, -70, 70},
};

void old_deploy_legs(void)
{
	action_servo(0, 60);
	action_servo(1, 60);
	action_servo(2, 90);

	action_servo(3, 90);
	action_servo(4, 60);
	action_servo(5, 90);

	action_servo(6, 120);
	action_servo(7, 60);
	action_servo(8, 90);


	action_servo(9, 120);
	action_servo(10, 120);
	action_servo(11, 90);

	action_servo(12, 90);
	action_servo(13, 120);
	action_servo(14, 90);

	action_servo(15, 60);
	action_servo(16, 120);
	action_servo(17, 90);
}


void deploy_legs(void)
{
	for (unsigned int i = 0; i < 6; i++) {
		move_leg(i, leg_base_position[i][0], leg_base_position[i][1], leg_base_position[i][2]);
	}
}



void put_legs_in_box(unsigned int step)
{
	for (unsigned int i = 0; i < 3; i++) {
		add_command(i, leg_base_position[i], (double[]){80, -60, 50}, 1000000, 40);
		add_command(i+3, leg_base_position[i+3], (double[]){-80, -60, 50}, 1000000, 40);
	}
	exec_command();
	wait_command();
}

void warming_up(void)
{
	move_leg_slowly(5, 20, 2, leg_base_position[5][0], leg_base_position[5][1], leg_base_position[5][2], leg_base_position[5][0]-50, leg_base_position[5][1]-50, leg_base_position[5][2]-50);
}

void start_walking(double x_step, double y_step, double height, double times)
{
	for (unsigned int i = 1; i < 6; i+=2) {
		add_command(i, leg_base_position[i], (double[]){leg_base_position[i][0]-x_step/2, leg_base_position[i][1]-y_step/2, leg_base_position[i][2]-height}, times, 40);
	}
	exec_command();
	wait_command();

	for (unsigned int i = 1; i < 6; i+=2) {
		add_command(i, (double[]){leg_base_position[i][0]-x_step/2, leg_base_position[i][1]-y_step/2, leg_base_position[i][2]-height}, (double[]){leg_base_position[i][0]-x_step/2, leg_base_position[i][1]-y_step/2, leg_base_position[i][2]}, times, 40);
	}
	exec_command();
	wait_command();

	for (unsigned int i = 0; i < 6; i+=2) {
		add_command(i, leg_base_position[i], (double[]){leg_base_position[i][0], leg_base_position[i][1], leg_base_position[i][2]-height}, times, 40);
	}
	exec_command();
	wait_command();
}




void walk(double x_step, double y_step, double height,  double times)
{
	for (unsigned int i = 0; i < 6; i+=2) {
		add_command(i, (double[]){leg_base_position[i][0]+x_step/2, leg_base_position[i][1]+y_step/2, leg_base_position[i][2]-height}, (double[]){leg_base_position[i][0]+x_step/2, leg_base_position[i][1]+y_step/2, leg_base_position[i][2]}, times, 40);
	}
	exec_command();
	wait_command();

	for (unsigned int i = 1; i < 6; i+=2) {
		add_command(i, (double[]){leg_base_position[i][0]-x_step/2, leg_base_position[i][1]-y_step/2, leg_base_position[i][2]}, (double[]){leg_base_position[i][0]-x_step/2, leg_base_position[i][1]-y_step/2, leg_base_position[i][2]-height}, times, 40);
	}
	exec_command();
	wait_command();

	for (unsigned int i = 0; i < 6; i+=2) {
	add_command(i, (double[]){leg_base_position[i][0]+x_step/2, leg_base_position[i][1]+y_step/2, leg_base_position[i][2]}, (double[]){leg_base_position[i][0]-x_step/2, leg_base_position[i][1]-y_step/2, leg_base_position[i][2]}, times, 40);
	}
	for (unsigned int i = 1; i < 6; i+=2) {
	add_command(i, (double[]){leg_base_position[i][0]-x_step/2, leg_base_position[i][1]-y_step/2, leg_base_position[i][2]-height}, (double[]){leg_base_position[i][0]+x_step/2, leg_base_position[i][1]+y_step/2, leg_base_position[i][2]-height}, times, 40);
	}
	exec_command();
	wait_command();

	for (unsigned int i = 1; i < 6; i+=2) {
		add_command(i, (double[]){leg_base_position[i][0]+x_step/2, leg_base_position[i][1]+y_step/2, leg_base_position[i][2]-height}, (double[]){leg_base_position[i][0]+x_step/2, leg_base_position[i][1]+y_step/2, leg_base_position[i][2]}, times, 40);
	}
	exec_command();
	wait_command();

	for (unsigned int i = 0; i < 6; i+=2) {
	add_command(i, (double[]){leg_base_position[i][0]-x_step/2, leg_base_position[i][1]-y_step/2, leg_base_position[i][2]}, (double[]){leg_base_position[i][0]-x_step/2, leg_base_position[i][1]-y_step/2, leg_base_position[i][2]-height}, times, 40);
	}
	exec_command();
	wait_command();


	for (unsigned int i = 1; i < 6; i+=2) {
	add_command(i, (double[]){leg_base_position[i][0]+x_step/2, leg_base_position[i][1]+y_step/2, leg_base_position[i][2]}, (double[]){leg_base_position[i][0]-x_step/2, leg_base_position[i][1]-y_step/2, leg_base_position[i][2]}, times, 40);
	}
	for (unsigned int i = 0; i < 6; i+=2) {
	add_command(i, (double[]){leg_base_position[i][0]-x_step/2, leg_base_position[i][1]-y_step/2, leg_base_position[i][2]-height}, (double[]){leg_base_position[i][0]+x_step/2, leg_base_position[i][1]+y_step/2, leg_base_position[i][2]-height}, times, 40);
	}
	exec_command();
	wait_command();
}



void grab_ball(double step)
{
	for (unsigned int i = 0; i < 6; i++) {
		move_leg(i, leg_base_position4[i][0], leg_base_position4[i][1], leg_base_position4[i][2]);
	}
	wait_servo(2);
	delay_s(1);
	action_servo(8, 60);
	action_servo(17, 120);
	wait_servo(2);
	action_servo(7, 120);
	action_servo(16, 60);
	wait_servo(2);
	action_servo(6, 180);
	action_servo(15, 0);
	wait_servo(2);
	delay_s(1);

	action_servo(7, 30);
	action_servo(16, 150);
	wait_servo(2);

	delay_s(2);
	//move_leg(2, 0, 0, 100);
	//move_leg(5, 0, 0, 100);
}



void walk4(double step)
{
	for (unsigned int i = 0; i < 6; i++) {
		move_leg(i, leg_base_position4[i][0], leg_base_position4[i][1], leg_base_position4[i][2]);
	}
}
