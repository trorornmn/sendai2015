#ifndef SERVO_H
#define SERVO_H


enum SERVO_STATUS {
	ON,
	OFF
};

struct position {
	double x, y, z;
};



struct command {
	unsigned char flag, move_flag;
	double target_time, rest_time;
	unsigned int target_step, current_step;
	double first_pos[3], last_pos[3];
};

void servo_onoff(unsigned int servo_i, enum SERVO_STATUS status);
void action_servo(unsigned int servo_i, unsigned char deg);
void wait_servo(double times);
void set_all_servo(enum SERVO_STATUS status);
void test_servo(unsigned char servo_pin, unsigned char step);
void add_command(unsigned int leg_pin, const double first_pos[3], const double last_pos[3],  double time, unsigned int step);
void exec_command(void);
void stop_command(void);
void wait_command(void);

#endif /* SERVO_H */
