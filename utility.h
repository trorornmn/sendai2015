#ifndef UTILITY_H
#define UTILITY_H

#include <stdint.h>

#define M_PI 3.14159265358979323846

void delay_us(unsigned int us);
void delay_ms(unsigned int msec);
void delay_s(unsigned int sec);
void pipipi(char count);
void pii(char count);
uint16_t read_ADC(uint8_t ADC_Channel);

#define digital_write(GPIOx, pin, value) (GPIOx->BSRR = (uint32_t)1 << ((value == 1) ? pin: pin+16))
#define digital_read(GPIOx, pin) (((GPIOx->IDR & 1 << pin) != 0) ? 1 : 0)

inline static double todegrees(double radians)
{
	return 180 * radians / M_PI;
}



inline static double toradians(double degrees)
{
	return M_PI * degrees / 180;
}

#endif /* UTILITY_H */
