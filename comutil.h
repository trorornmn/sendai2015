#ifndef COMUTIL_H
#define COMUTIL_H

uint16_t uputchar(uint16_t c);
int uputs(const char *s);
int uprintf(const char *format, ...);

#endif /* COMUTIL_H */
