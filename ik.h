#ifndef IK_H
#define IK_H

#include <math.h>

#define TARSUS_DEG 9.3099407313024
#define TARSUS_LEN 123.6284756842
#define FEMURA_LEN 45

inline static double lawofcosines(double b, double c, double alpha)
{
	return sqrt(b*b+c*c - 2*b*c*cos(alpha));
}



inline static double lawofcosines2(double a, double b, double c)
{
	return (b*b + c*c - a*a) / (2*b*c);
}

void move_leg_xz(unsigned int leg_i, double x, double z);
void move_leg(unsigned int leg_i, double x, double y, double z);
void move_leg_slowly(unsigned int leg_i, unsigned int step, double times, double old_x, double old_y, double old_z, double x, double y, double z);
void calc_next_pos(unsigned int current_step, unsigned int target_step, const double old_pos[3], const double new_pos[3], double *target_pos);

#endif /* IK_H */
